/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ifcoder.wekaAssociation;


import java.awt.Desktop;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 *
 * @author jose
 */
public class util {      

    public static String getBarraSO() {
        String nome = System.getProperty("os.name");
        if (nome.contains("Linux")) {
            return "/";
        } else {
            return "\\";
        }
    }    

    /**
     *
     * @param fullPath
     * @return retorna o caminho da pasta onde se encontra o arquivo
     */
    public static String getPastaArquivo(String fullPath) {
        String caminhoPreferenciaDaEstacao = "";
        if (fullPath.contains("\\")) { //windowns
            caminhoPreferenciaDaEstacao = fullPath.substring(0, fullPath.lastIndexOf("\\") + 1);
        } else {//linux        
            caminhoPreferenciaDaEstacao = fullPath.substring(0, fullPath.lastIndexOf("/") + 1);
        }
        return caminhoPreferenciaDaEstacao;
    }

    public static void writeArquivo(String caminho, String texto) throws IOException {
        FileWriter arq = new FileWriter(caminho);
        PrintWriter gravarArq = new PrintWriter(arq);
        gravarArq.print(texto);
        arq.close();
    }

    public static boolean arquivoExiste(String caminho) {
        File f = new File(caminho);
        if (f.exists()) {
            return true;
        } else {
            return false;
        }
    }

    public static Scanner openFile(String caminho) throws FileNotFoundException {
        Scanner arquivo = null;
        if (arquivoExiste(caminho)) {
            FileReader f = null;
            f = new FileReader(caminho);
            arquivo = new Scanner(f);
        }

        return arquivo;
    }

    public static void openPDF(String caminhoPDF) {
        if (Desktop.isDesktopSupported()) {            
            try {
                File myFile = new File(caminhoPDF);
                Desktop.getDesktop().open(myFile);
            } catch (IOException ex) {
                Logger.getLogger(util.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        }
    }

    public static Boolean saveFile(String caminho, String dadosArquivo) throws FileNotFoundException {
        FileWriter arq = null;
        try {
            arq = new FileWriter(caminho);
            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.print(dadosArquivo);
            arq.close();
            return true;
        } catch (IOException ex) {
            Logger.getLogger(util.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            try {
                arq.close();
            } catch (IOException ex) {
                Logger.getLogger(util.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

   
    /**
     *
     * @param formulario
     * @param mensagemCabecalho
     * @param abrir: true é dialogo de abrir arquivo, false é para salvar
     * arquivo
     * @param projeto
     * @return
     */
    public static String escolherArquivo(JFrame formulario, String mensagemCabecalho, boolean abrir) {
        JFileChooser chooserFile = new JFileChooser();
        chooserFile.setDialogTitle(mensagemCabecalho);
        //chooser.setMultiSelectionEnabled(true);

        //FileNameExtensionFilter filter = new FileNameExtensionFilter("*.TXT","*.txt", "*.mdb");
        //fchoEstacao.setFileFilter(filter);
        String nomeArq = "";
        String caminhoSugerido = "";

        File f = new File(caminhoSugerido);
        if (!f.exists()) {
            f.mkdir();
        }
        chooserFile.setCurrentDirectory(f);

        int returnVal;
        if (abrir) {
            returnVal = chooserFile.showOpenDialog(formulario);
        } else { //salvar
            if (!nomeArq.equals("")) {
                chooserFile.setSelectedFile(new File(nomeArq + ".Prj"));
            }
            returnVal = chooserFile.showSaveDialog(formulario);
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = chooserFile.getSelectedFile();
            String path = file.getAbsolutePath();

            System.out.println("Arquivo acessado " + path);
            return path;
        } else {
            System.out.println("Access canceled.");
            return "";
        }
    }

    public static String escolherArquivo(JDialog formulario, String mensagemCabecalho, boolean abrir) {
        JFileChooser chooserFile = new JFileChooser();
        chooserFile.setDialogTitle(mensagemCabecalho);

        String nomeArq = "";
        String caminhoSugerido = "";

        File f = new File(caminhoSugerido);
        chooserFile.setCurrentDirectory(f);

        int returnVal;
        if (abrir) {
            returnVal = chooserFile.showOpenDialog(formulario);
        } else { //salvar
            if (!nomeArq.equals("")) {
                chooserFile.setSelectedFile(new File(nomeArq + ".Prj"));
            }
            returnVal = chooserFile.showSaveDialog(formulario);
        }
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = chooserFile.getSelectedFile();
            String path = file.getAbsolutePath();

            System.out.println("Arquivo acessado " + path);
            return path;
        } else {
            System.out.println("Access canceled.");
            return "";
        }
    }

    

    public static String escolherPasta(String mensagemCabecalho) {
        String arquivoWave;
        JFileChooser arquivo = new JFileChooser();
        arquivo.setDialogTitle(mensagemCabecalho);

        String caminhoSugerido = "";

        File f = new File(caminhoSugerido);
        if (!f.exists()) {
            f.mkdir();
        }
        arquivo.setCurrentDirectory(f);

        arquivo.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if (arquivo.showOpenDialog(arquivo) == JFileChooser.APPROVE_OPTION) {
            arquivoWave = arquivo.getSelectedFile().getPath();
        } else {
            arquivoWave = "";
        }
        return arquivoWave;
    }

   

    
}
